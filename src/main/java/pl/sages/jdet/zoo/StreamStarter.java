package pl.sages.jdet.zoo;

import pl.sages.jdet.zoo.animals.Fish;
import pl.sages.jdet.zoo.animals.Oyster;
import pl.sages.jdet.zoo.animals.bird.Eagle;
import pl.sages.jdet.zoo.animals.fish.Shark;
import pl.sages.jdet.zoo.animals.mammal.Bear;
import pl.sages.jdet.zoo.animals.mammal.Horse;
import pl.sages.jdet.zoo.animals.mammal.PolarBear;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamStarter {

    public static void main(String[] args) {
        System.out.println("StreamStarter.main");

        Bear b = new Bear(123, "Puchatek");
        List<Animal> animals = new ArrayList<>();
        animals.add(b);
        animals.add(new Horse(234, "Kasztanka"));
        animals.add(new Eagle(12, "Bielik"));
        animals.add(new Shark(99, "Janusz"));
        animals.add(b);
        animals.add(new Oyster(1, "Ostra"));
        animals.add(new PolarBear(323, "Iwan"));

        Consumer<String> stringConsumer = System.out::println;
                //n -> System.out.println(n);

        Stream<Animal> stream = animals.parallelStream();

        if(!stream.isParallel()){
            stream = stream.parallel();
        }

        if(stream.isParallel()){
            stream = stream.sequential();
        }

        //animals =
        //long count = 
        //boolean match =

        Optional<String> optionalString = 
                prepare(stream)
                // terminal operations
                .findFirst();
                //.collect( Collectors.toList());
                //.count();
                //.noneMatch( n -> n.contains("x") ); // java.util.function.Predicate
                //.forEach(a -> System.out.println(a)); // java.util.function.Consumer


        Supplier<RuntimeException> exceptionSupplier = () -> new IllegalStateException("fake exception");
        String name = optionalString.orElseThrow(exceptionSupplier); // java.util.function.Supplier
        System.out.println("name = " + name);
        optionalString.ifPresent(s-> System.out.println(s));


        Optional<String> namesTogether = prepare(animals.stream()).reduce((n1,n2)-> n1 + ", " +  n2);
        namesTogether.ifPresent(System.out::println);

        Optional<Integer> optionalCount = prepare(animals.stream())
                .map(n->n.length()).reduce((l1,l2)->l1+l2);

        long count = prepare(animals.stream()).flatMap(s-> Arrays.stream(s.split(""))).count();

        System.out.println("count=" + count + ", count2=" + optionalCount.get());

        Stream<String> stringStream = Stream.iterate("1", s->s+"1");
                //.generate(()->"spring");
        stringStream.limit(10).forEach(System.out::println);

    }

    public static Stream<String> prepare(Stream<Animal> stream){

        return stream.filter(a -> a.getSize()>=50) // java.util.function.Predicate
                .sorted( (a1, a2) ->  a1.getName().compareTo(a2.getName())) // java.util.Comparator
                .distinct()
                //.limit(3)
                .map(a->a.getName()); // java.util.function.Function

    }
}
