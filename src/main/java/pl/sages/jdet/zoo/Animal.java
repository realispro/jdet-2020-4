package pl.sages.jdet.zoo;

import java.util.Objects;

public abstract class Animal implements Comparable<Animal>{

    @Positive
    private int size;

    @NotNull
    private String name;



    public Animal(int size, String name) {
        this.size = size;
        this.name = name;
    }

    public void eat(String food){
        System.out.println("animal is eating " + food);
    }

    @Deprecated
    public abstract void move();

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Animal a) {
        return this.size - a.size;
                //this.size > a.size ? 1 : -1;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return size == animal.size &&
                name.equals(animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, name);
    }

    @Override
    public String toString() {
        return "Animal{" +
                "size=" + size +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("destroying animal");
        super.finalize();
    }
}
