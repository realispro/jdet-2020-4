package pl.sages.jdet.zoo;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import pl.sages.jdet.util.Container;
import pl.sages.jdet.zoo.animals.bird.Eagle;
import pl.sages.jdet.zoo.animals.fish.Shark;
import pl.sages.jdet.zoo.animals.mammal.Bear;
import pl.sages.jdet.zoo.animals.mammal.PolarBear;
import pl.sages.jdet.zoo.habitat.Aquarium;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class ZooStarter {

    public static void main(String[] args) {
        System.out.println("ZooStarter.main");

        Animal a = new Bear(200, "Bartek");

        List<Animal> animalCollection = new ArrayList<>();
        animalCollection.add(a);
        animalCollection.add(new Shark(100, "Szczeki"));
        animalCollection.add(new Eagle(20, "Bielik"));
        animalCollection.add(new Bear(200, "Bartek"));

        System.out.println("contains: " + animalCollection.contains(new Bear(200, "Bartek")));
        System.out.println("size: " + animalCollection.size());

        animalCollection.sort((a1, a2) -> a1.getSize() - a2.getSize());

        animalCollection.removeIf(animal -> animal.getSize()>150);
        
        animalCollection.forEach(System.out::println);
        
        BiMap<String, Animal> map = HashBiMap.create();
                //new TreeMap<>(new KeyComparator());

        map.put("Marcin", new Bear(250, "John"));
        map.put("TomekKa", new Shark(111, "Krzysztof"));
        map.put("Filip", new Eagle(12, "Bielik"));
        map.put("TomekGie", new Bear(350, "Polar"));
        map.put("Filip", new Shark(123, "Janusz"));

        map.remove("Marcin");

        Animal animalFromMap = map.get("Marcin");
        System.out.println("animalFromMap = " + animalFromMap);

        for(Map.Entry<String, Animal> entry : map.entrySet()){
            System.out.println("key=" + entry.getKey() + ", value=" + entry.getValue());
        }

        for( String key : map.keySet()){
            System.out.println("key=" + key + ", value=" + map.get(key));
        }
        
        BiMap<Animal, String> inversedMap = map.inverse();
        String worker = inversedMap.get(new Bear(350, "Polarny"));
        System.out.println("worker = " + worker);

        List<Bear> bears = new ArrayList<>();
        bears.add(new Bear(123, "Puchatek"));
        bears.add(new Bear(234, "Uszatek"));

        readAnimals(bears);
        addAnimal(bears);

        Container<Bear> container = new Container<>();
        container.setO(new Bear(12, "Koralgol"));
        Bear b = container.getO();
        System.out.println("b = " + b);

        //Pair<String, Animal> pair = new Pair<>();
        PairAnimal<String> pair = new PairAnimal<>();

        pair.setOne("Marcin");
        pair.setTwo(new Shark(123, "Ludwik"));
        String w = pair.getOne();
        Animal a1 = pair.getTwo();

        System.out.println("w=" + w + ", a1 = " + a1);

        Shark shark = new Shark(123, "Janusz");
        Aquarium habitat = new Aquarium();
        habitat.setO(shark);

    }

    // PECS - Producer Extends, Consumer Super
    public static void readAnimals(List<? extends Animal> animals){
        for(Animal a : animals){
            System.out.println("a = " + a);
        }
    }

    public static void addAnimal(List<? super PolarBear> animals){
        animals.add(new PolarBear(345, "Iwan"));
    }

}
