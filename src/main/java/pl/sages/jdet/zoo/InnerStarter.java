package pl.sages.jdet.zoo;

import pl.sages.jdet.common.Person;
import pl.sages.jdet.travel.Transportation;
import pl.sages.jdet.zoo.animals.Oyster;

public class InnerStarter {

    public static void main(String[] args) {
        System.out.println("InnerStarter.main");

        Oyster oyster = new Oyster(1, "malz");
        String sand = "sand grain";

        Oyster.Pearl pearl = oyster.new Pearl(sand);
        System.out.println("value=" + pearl.getValue());

        oyster = null;

        Oyster.InnerStaticClass innerStaticClass = new Oyster.InnerStaticClass("param");

        class LocalClass implements Transportation {
            @Override
            public void transport(Person passenger) {

            }
        }

        LocalClass localClass = new LocalClass();

        System.out.println("done.");

    }
}
