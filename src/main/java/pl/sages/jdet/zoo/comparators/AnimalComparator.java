package pl.sages.jdet.zoo.comparators;

import pl.sages.jdet.zoo.Animal;

import java.util.Comparator;

public class AnimalComparator implements Comparator<Animal> {
    @Override
    public int compare(Animal a1, Animal a2) {
        return a1.getName().compareTo(a2.getName());
        //return a2.getSize()-a1.getSize();
    }
}
