package pl.sages.jdet.zoo.comparators;

import java.util.Comparator;

public class KeyComparator implements Comparator<String> {
    @Override
    public int compare(String o1, String o2) {
        return o2.length()-o1.length();
    }
}
