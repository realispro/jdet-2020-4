package pl.sages.jdet.zoo.animals.mammal;

import pl.sages.jdet.zoo.Predator;
import pl.sages.jdet.zoo.animals.mammal.Bear;

@Predator(1)
public class PolarBear extends Bear {

    public PolarBear(int size, String name) {
        super(size, name);
    }
}
