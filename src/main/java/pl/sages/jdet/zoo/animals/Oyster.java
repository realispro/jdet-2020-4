package pl.sages.jdet.zoo.animals;

import pl.sages.jdet.zoo.Animal;

public class Oyster extends Animal {

    private String resource = "some resource";

    private static String staticResource = "some static resource";

    public Oyster(int size, String name) {
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("oyster is crawling");
    }


    public class Pearl {

        public Pearl(String sand){
            System.out.println("constructing pearl of " + sand + " and " + resource + " and " + staticResource);
        }

        public int getValue(){
            return 30;
        }
    }

    public static class InnerStaticClass {

        public  InnerStaticClass(String param){
            System.out.println("constructing innerstaticclass of " + param + " and " + staticResource);
        }

    }

}
