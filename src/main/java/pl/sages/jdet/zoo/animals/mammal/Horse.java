package pl.sages.jdet.zoo.animals.mammal;

import pl.sages.jdet.zoo.animals.Mammal;

public class Horse extends Mammal {
    public Horse(int size, String name) {
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("horse is running !!!");
    }
}
