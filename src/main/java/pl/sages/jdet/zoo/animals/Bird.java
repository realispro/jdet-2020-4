package pl.sages.jdet.zoo.animals;

import pl.sages.jdet.zoo.Animal;

public abstract class Bird extends Animal {
    public Bird(int size, String name) {
        super(size, name);
    }
}
