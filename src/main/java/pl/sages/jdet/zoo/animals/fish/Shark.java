package pl.sages.jdet.zoo.animals.fish;

import pl.sages.jdet.zoo.Predator;
import pl.sages.jdet.zoo.animals.Fish;

@Predator(2)
public class Shark extends Fish {

    public Shark(int size, String name){
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("Shark is swimming");
    }
}
