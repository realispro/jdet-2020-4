package pl.sages.jdet.zoo.animals.mammal;

import pl.sages.jdet.zoo.animals.Mammal;

public class Bear extends Mammal {

    public Bear(int size, String name) {
        super(size, name);
    }

    @Override
    public void move() {
        System.out.println("bear is running");
    }
}
