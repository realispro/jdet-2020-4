package pl.sages.jdet.zoo.animals;

import pl.sages.jdet.zoo.Animal;

public abstract class Fish extends Animal {
    public Fish(int size, String name) {
        super(size, name);
    }
}
