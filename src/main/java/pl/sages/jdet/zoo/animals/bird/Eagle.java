package pl.sages.jdet.zoo.animals.bird;

import pl.sages.jdet.zoo.Animal;
import pl.sages.jdet.zoo.Init;
import pl.sages.jdet.zoo.Predator;
import pl.sages.jdet.zoo.animals.Bird;

@Deprecated
@Predator
public class Eagle extends Bird {


    public Eagle(int size, String name) {
        super(size, name);
    }

    @Init
    public void abc(){
        System.out.println("eagle initialization");
    }


    @Override
    @Deprecated
    public void move() {
        System.out.println("eagle is flying");
    }
}
