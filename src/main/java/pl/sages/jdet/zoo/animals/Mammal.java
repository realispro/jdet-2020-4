package pl.sages.jdet.zoo.animals;

import pl.sages.jdet.zoo.Animal;

public abstract class Mammal extends Animal {
    public Mammal(int size, String name) {
        super(size, name);
    }
}
