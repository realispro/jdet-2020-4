package pl.sages.jdet.time;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeStarter {

    public static void main(String[] args) {
        System.out.println("TimeStarter.main");

        Locale locale = new Locale("pl", "PL");
        Locale.setDefault(locale);

        // JAVA 1.1 -> present
        Date date = new Date();
        long millis = date.getTime(); // 1.1.1970 == EPOCH TIME

        Calendar calendar = Calendar.getInstance();
        //calendar.setTime(date);
        calendar.set(2020, 12, 17, 11, 37);
        //calendar.roll(Calendar.MONTH, 10);
        date = calendar.getTime();

        DateFormat format = new SimpleDateFormat("yyyy%MM%dd w W G");
                // DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);

        // JAVA 8 -> present : java.time
        LocalDate ld = LocalDate.now();
        LocalTime lt = LocalTime.now();
        LocalDateTime ldt = LocalDateTime.of(2020, 4, 18, 12, 12);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy%MM%dd HH:mm w W X");

        ZonedDateTime warsawTime = ldt.atZone(ZoneId.of("Europe/Warsaw"));

        ZonedDateTime singaporeTime = warsawTime
                .withZoneSameInstant(ZoneId.of("Asia/Singapore"))
                .plusHours(10);

        Period p = Period.between(warsawTime.toLocalDate(), singaporeTime.toLocalDate());
        System.out.println("p.getMonths() = " + p.getDays());
        
        Duration d = Duration.between(warsawTime, singaporeTime);
        System.out.println("d = " + d);


        String timestamp = singaporeTime.format(formatter);

        System.out.println("[" + timestamp + "] sample message");

        LocalDate now = LocalDate.now();
        LocalDate christmasEve =  LocalDate.of(2020, Month.DECEMBER, 24);

        long days = ChronoUnit.DAYS.between(now, christmasEve);
                //Duration.between(now, christmasEve).toDays();

        System.out.println("days = " + days);


    }
}
