package pl.sages.jdet.reflect;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class DurationInvocationHandler implements InvocationHandler {

    Object target;

    public DurationInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        long start = System.nanoTime();
        Object r = method.invoke(target, args);
        long duration = System.nanoTime() - start;
        System.out.println("method " + method.getName() + " invocation took " + duration + " nanos");
        return r;
    }
}
