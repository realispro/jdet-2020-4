package pl.sages.jdet.reflect;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.time.LocalTime;

public class TimeInvocationHandler implements InvocationHandler {

    private int opening;

    private int closing;

    private Object target;

    public TimeInvocationHandler(int opening, int closing, Object target) {
        this.opening = opening;
        this.closing = closing;
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        int hour = LocalTime.now().getHour();

        if(opening<=hour && closing>hour){
            return method.invoke(target, args);
        } else {
            throw new RuntimeException("system closed.");
        }
    }
}
