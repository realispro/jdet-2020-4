package pl.sages.jdet.reflect;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.Arrays;

public class ClassViewer {

    private Class clazz;

    public ClassViewer(Class clazz) {
        this.clazz = clazz;
    }

    public void printClassSignature(){

        Annotation[] annotations = clazz.getAnnotations();
        String annoString = Arrays.stream(annotations).map(a->a.toString()).reduce((n1,n2)->n1+", "+n2).orElse("");
        System.out.println(annoString);
        System.out.println(Modifier.toString(clazz.getModifiers()) +
                " " + clazz.getName() );
        Class[] interfaces = clazz.getInterfaces();
        Arrays.stream(interfaces).forEach(c->System.out.println("interface: " + c.getName()));
    }

    public void printClassFields(){
        Field[] fields = clazz.getDeclaredFields();
        Arrays.stream(fields).forEach( f -> {
            System.out.println("field: " + Modifier.toString(f.getModifiers()) + " " + f.getType().getName() + " " + f.getName());
        });
    }

    public void printClassConstructors(){
        Constructor[] constructors = clazz.getConstructors();
        Arrays.stream(constructors).forEach(c->{
            String exceptions = getClassesString(c.getExceptionTypes());
            if(exceptions.length()>0){
                exceptions = " throws " + exceptions;
            }
            System.out.println("constructor: " + Modifier.toString(c.getModifiers()) +
                    " " + clazz.getSimpleName() + "(" + getParametersString(c.getParameters()) + ")" + exceptions );
        });
    }

    public void printClassMethods(){
        Method[] methods = clazz.getDeclaredMethods();
        Arrays.stream(methods).forEach(m->{
            String exceptions = getClassesString(m.getExceptionTypes());
            if(exceptions.length()>0){
                exceptions = " throws " + exceptions;
            }
            Annotation[] annotations = m.getAnnotations();
            String annoString = Arrays.stream(annotations).map(a->a.toString()).reduce((n1,n2)->n1+", "+n2).orElse("");


            System.out.println(annoString);
            System.out.println("method: " + Modifier.toString(m.getModifiers()) +
                    " " + m.getReturnType().getName() +
                    " " + m.getName() +
                    "(" + getParametersString(m.getParameters()) + ")"
                    + exceptions
                    );

        });
    }

    private String getClassesString(Class[] classes){
        return Arrays.stream(classes)
                .map(c->c.getSimpleName())
                .reduce((n1,n2)->n1+", " + n2)
                .orElse("");
    }

    private String getParametersString(Parameter[] params){
        // String, String
        return Arrays.stream(params)
                .map(p->p.toString())
                .reduce((n1,n2)->n1+", "+n2)
                .orElse("");
    }

}
