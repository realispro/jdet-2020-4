package pl.sages.jdet.reflect;

import pl.sages.jdet.zoo.NotNull;
import pl.sages.jdet.zoo.Positive;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Validator {

    public void validate(Object o) throws IllegalStateException{

        getAllFields(o.getClass()).forEach(f->{
            try {

                if (f.isAnnotationPresent(NotNull.class)) {
                    f.setAccessible(true);
                    Object r = f.get(o);
                    if (r == null) {
                        throw new IllegalStateException("field " + f.getName() + " must be not null");
                    } else {
                        System.out.println("field " + f.getName() + " successfully validated against not null");
                    }
                }
                if (f.isAnnotationPresent(Positive.class)) {
                    f.setAccessible(true);
                    Object r = f.get(o);
                    if (Integer.parseInt(String.valueOf(r))<0) {
                        throw new IllegalStateException("field " + f.getName() + " must be positive");
                    } else {
                        System.out.println("field " + f.getName() + " successfully validated against positivity");
                    }
                }
            }catch (IllegalAccessException e){
                throw new RuntimeException("problem whle validation", e);
            }
        });
    }

    private Stream<Field> getAllFields(Class clazz){
        List<Field> fieldList = new ArrayList<>();
        do {
            Field[] fields = clazz.getDeclaredFields();
            fieldList.addAll(Arrays.asList(fields));
            clazz = clazz.getSuperclass();
        }while(clazz!=null);
        return fieldList.stream();
    }

}
