package pl.sages.jdet.reflect;

import pl.sages.jdet.zoo.Animal;
import pl.sages.jdet.zoo.Init;
import pl.sages.jdet.zoo.Predator;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectionStarter {


    public static void main(String[] args) {
        System.out.println("ReflectionStarter.main");

        String className = args[0];
        int size = Integer.parseInt(args[1]);
        String name = args.length>2 ? args[2] : null;

        try {
            Class animalClass = Class.forName(className);
            Predator p = (Predator)animalClass.getAnnotation(Predator.class);
            if(p!=null){
                System.out.println("WARNING! about to create predator! Be careful! Priority=" + p.value());
            }

            Constructor animalConstructor = animalClass.getConstructor(int.class, String.class);
            Object o = animalConstructor.newInstance(size, name);

            if(o instanceof Animal) {
                Animal a = (Animal) o;

                Validator v = new Validator();
                v.validate(a);

                Arrays.stream(animalClass.getMethods())
                        .filter(m->m.isAnnotationPresent(Init.class))
                        .findAny()
                        .ifPresent(m->{
                            try {
                                m.invoke(a);
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                throw new RuntimeException("init problem", e);
                            }
                        });
                
                Field field = Animal.class.getDeclaredField("size");

                field.setAccessible(true);
                Object fieldValue = field.get(a);
                System.out.println("fieldValue = " + fieldValue);

                field.set(a, 321);
                
                System.out.println("created animal: " + a);
                a.eat("chips");

                //a.move();
                Method method = animalClass.getMethod("move");
                method.invoke(a);

            } else {
                System.out.println("created object is not an instance of Animal class");
            }

        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchFieldException e) {
            e.printStackTrace();
        }

    }

}
