package pl.sages.jdet.reflect;

import pl.sages.jdet.project.Developer;
import pl.sages.jdet.travel.Plane;
import pl.sages.jdet.zoo.Animal;
import pl.sages.jdet.zoo.animals.mammal.Bear;

import java.util.function.Supplier;

public class IntrospectionStarter {

    public static void main(String[] args) {
        System.out.println("ReflectionStarter.main");

        Supplier<Class> classSupplier = getClassSupplier();

        Class clazz = classSupplier.get();

        do {
            System.out.println("**********************");
            ClassViewer viewer = new ClassViewer(clazz);

            viewer.printClassSignature();
            viewer.printClassFields();
            viewer.printClassConstructors();
            viewer.printClassMethods();

            clazz = clazz.getSuperclass();
        }while (clazz!=null && clazz!=Object.class);



    }


    public static Supplier<Class> getClassSupplier(){
        return () -> {
            try {
                return Class.forName("pl.sages.jdet.project.Developer");
            } catch (ClassNotFoundException e) {
                throw new RuntimeException("missing class", e);
            }
        };
        // Developer.class;
        // o.getClass();
    }

}
