package pl.sages.jdet.util;

public class Container<T extends Object> {

    private T o;

    public T getO() {
        return o;
    }

    public void setO(T o) {
        this.o = o;
    }
}
