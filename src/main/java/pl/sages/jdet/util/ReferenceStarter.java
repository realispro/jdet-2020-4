package pl.sages.jdet.util;

import pl.sages.jdet.zoo.Animal;
import pl.sages.jdet.zoo.animals.mammal.Bear;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

public class ReferenceStarter {

    public static void main(String[] args) {
        System.out.println("ReferenceStarter.main");


        for(int i=0; i<100000; i++) {
            //Animal a = new Bear(123, "Iwan"); //normal reference

            Reference<Animal> ref = new PhantomReference<>(new Bear(123, "Iwan"), null);
                    // new WeakReference<>(new Bear(123, "Iwan"));
                    // new SoftReference<>(new Bear(123, "Iwan")); // OutOfMemoryError
            //ref = null;
            Animal a = ref.get();


        }
        System.gc();

        try {
            Thread.sleep(1000 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
