package pl.sages.jdet.util;

public class Pair<T, U> {

    private T one;

    private U two;


    public T getOne() {
        return one;
    }

    public void setOne(T one) {
        this.one = one;
    }

    public U getTwo() {
        return two;
    }

    public void setTwo(U two) {
        this.two = two;
    }
}
