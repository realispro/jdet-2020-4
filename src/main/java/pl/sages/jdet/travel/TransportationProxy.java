package pl.sages.jdet.travel;

import pl.sages.jdet.common.Person;

import java.time.LocalTime;

public class TransportationProxy implements Transportation {

    private int opening;

    private int closing;

    private Transportation t;

    public TransportationProxy(int opening, int closing, Transportation t) {
        this.opening = opening;
        this.closing = closing;
        this.t = t;
    }

    @Override
    public void transport(Person passenger) {
        int hour = LocalTime.now().getHour();
        if(opening<=hour && closing>hour){
            t.transport(passenger);
        } else {
            throw new RuntimeException("system closed");
        }
    }
}
