package pl.sages.jdet.travel;

import pl.sages.jdet.common.Person;

@FunctionalInterface
public interface Transportation {

    void transport(Person passenger);

    default int getSpeed(){ // JAVA 8
        return 0;
    }

    static String getDescription(){ // JAVA 8
        return "some description ";
    }



}
