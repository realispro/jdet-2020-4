package pl.sages.jdet.travel;

import pl.sages.jdet.common.Person;
import pl.sages.jdet.reflect.DurationInvocationHandler;
import pl.sages.jdet.reflect.TimeInvocationHandler;

import java.lang.reflect.Proxy;

public class TravelStarter {

    public static void main(String[] args) {
        System.out.println("TravelStarter.main");

        Person kowalski = new Person("Jan", "Kowalski");

        Transportation t = System.out::println;
        //p -> System.out.println(p);

        t = (Transportation) Proxy.newProxyInstance(
                Transportation.class.getClassLoader(),
                new Class[]{Transportation.class},
                new TimeInvocationHandler(9, 17, t)
        );
        //t = new TransportationProxy(9, 16, t);
        t = (Transportation) Proxy.newProxyInstance(
                Transportation.class.getClassLoader(),
                new Class[]{Transportation.class},
                new DurationInvocationHandler(t)
        );

        t.transport(kowalski);
        System.out.println("travelling with speed " + t.getSpeed());

        System.out.println("done.");

    }
}
