package pl.sages.jdet.travel;

import pl.sages.jdet.common.Person;

public class Plane implements Transportation {

    @Override
    public void transport(Person passenger) {
        System.out.println("Passenger " + passenger + " is traveling by plane");
    }

    @Override
    public int getSpeed() {
        return 1000;
    }




}
