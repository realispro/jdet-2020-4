package pl.sages.jdet.project;

import java.util.ArrayList;
import java.util.List;

public class ProjectStarter {

    public static void main(String[] args) {
        System.out.println("ProjectStarter.main");

        Developer dev1 = new Developer("Joe", "Doe");
        dev1.addSkill(Skill.JAVA);
        dev1.addSkill(Skill.JPA);
        dev1.addSkill(Skill.SPRING);

        Developer dev2 = new Developer("John", "Smith");
        dev2.addSkill(Skill.JAVASCRIPT);
        dev2.addSkill(Skill.ANGULAR);

        Developer dev3 = new Developer("Jack", "Black");
        dev3.addSkill(Skill.JAVA);
        dev3.addSkill(Skill.JPA);
        dev3.addSkill(Skill.JAVASCRIPT);
        dev3.addSkill(Skill.REACT_JS);

        List<Developer> devList = new ArrayList<>();
        devList.add(dev1);
        devList.add(dev2);
        devList.add(dev3);

        boolean backend =
                devList.stream()
                        .sorted((d1,d2)->(d1.getSkills().size() - d2.getSkills().size()))
                        .limit(1)
                        .peek(d -> System.out.println(d))
                        .anyMatch(d -> d.isBackend());
        System.out.println("backend = " + backend);


        devList.stream()
                .flatMap(d->d.getSkills().stream())
                .distinct()
                .forEach(s-> System.out.println(s));

    }

}
