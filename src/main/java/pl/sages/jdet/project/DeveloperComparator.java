package pl.sages.jdet.project;

import java.util.Comparator;

public class DeveloperComparator implements Comparator<Developer> {

    private boolean asc;

    public DeveloperComparator(boolean asc) {
        this.asc = asc;
    }

    @Override
    public int compare(Developer d1, Developer d2) {
        return asc ?
                d1.getSkills().size() - d2.getSkills().size() :
                d2.getSkills().size() - d1.getSkills().size();
    }
}
