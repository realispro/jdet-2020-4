package pl.sages.jdet.project;

import pl.sages.jdet.common.Person;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class Developer extends Person {

    private Set<Skill> skills = new HashSet<>();

    public Developer(String firstName, String lastName) throws IllegalArgumentException{
        super(firstName, lastName);
    }

    public void addSkill(Skill skill) throws IllegalStateException{
        skills.add(skill);
   }

    public Set<Skill> getSkills() {
        return Collections.unmodifiableSet(skills);
    }

    public boolean isFrontend(){
       for(Skill skill : skills){
           if(!skill.isBackend()){
               return true;
           }
       }
       return false;
    }

    public boolean isBackend(){
        for(Skill skill : skills){
            if(skill.isBackend()){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "skills=" + skills +
                '}';
    }
}
